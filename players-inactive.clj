
[{:name "AC Feldt", :start-rating 1500.93}
 {:name "AT Karaman", :start-rating 1500.25}
 {:name "Adam Frydman", :start-rating 1476.0}
 {:name "Adam Sandberg Lööf", :start-rating 1492.64}
 {:name "Aidin Geranmayeh", :start-rating 1495.65}
 {:name "Alex Bäckgren", :start-rating 1500.0}
 {:name "Ali Azarbad", :start-rating 1500.0}
 {:name "Ali Cicek", :start-rating 1464.16}
 {:name "Ali Kiruk", :start-rating 1486.87}
 {:name "Anders Blixt", :start-rating 1495.52}
 {:name "Anders N", :start-rating 1500.0}
 {:name "Anders Uselius", :start-rating 1500.0}
 {:name "Andreas", :start-rating 1500.0}
 {:name "Andreas Engström", :start-rating 1493.87}
 {:name "Anna Jändel", :start-rating 1496.53}
 {:name "Anna-Maj", :start-rating 1496.0}
 {:name "Anton Lagerbäck", :start-rating 1494.42}
 {:name "Ara Matevoorian", :start-rating 1500.0}
 {:name "Armik Aghakhan", :start-rating 1481.29}
 {:name "Atilla Karan", :start-rating 1496.0}
 {:name "Basel Aiesh", :start-rating 1500.0}
 {:name "Bertil Söderberg", :start-rating 1496.0}
 {:name "Björn Fröding", :start-rating 1481.78}
 {:name "Björn Malmström", :start-rating 1500.0}
 {:name "Bulent Balikci", :start-rating 1500.0}
 {:name "Carin", :start-rating 1496.0}
 {:name "Casper", :start-rating 1500.0}
 {:name "Charlott", :start-rating 1496.0}
 {:name "Christofer Ricciuti", :start-rating 1500.0}
 {:name "Dan Sandberg", :start-rating 1503.34}
 {:name "Daniel Haffo", :start-rating 1493.65}
 {:name "David", :start-rating 1495.23}
 {:name "Denho Haffo", :start-rating 1500.0}
 {:name "Dennis Nilsson", :start-rating 1748.58}
 {:name "Einar Tryggvason", :start-rating 1526.75}
 {:name "Elias Håkansson", :start-rating 1474.48}
 {:name "Elis Sadjak", :start-rating 1500.57}
 {:name "Enny", :start-rating 1484.0}
 {:name "Ercan Kirik", :start-rating 1494.63}
 {:name "Erik Zetterman", :start-rating 1500.0}
 {:name "Ersan Tume", :start-rating 1500.0}
 {:name "Ersin", :start-rating 1486.22}
 {:name "Fredrik", :start-rating 1494.0}
 {:name "Fredrik Hoff", :start-rating 1500.0}
 {:name "Fritiof Sinclair", :start-rating 1500.0}
 {:name "Gustav Hannerz", :start-rating 1482.02}
 {:name "Göran Heldersten", :start-rating 1492.0}
 {:name "Göran Karlsson", :start-rating 1620.27}
 {:name "Hamid Shahbaz", :start-rating 1433.68}
 {:name "Hanna Pettersson", :start-rating 1496.73}
 {:name "Hanne Konradsen", :start-rating 1489.19}
 {:name "Hannes Hällgren", :start-rating 1497.0}
 {:name "Hassan Aram", :start-rating 1500.0}
 {:name "Henrik Karlsson", :start-rating 1495.69}
 {:name "Hesam", :start-rating 1488.0}
 {:name "Håkan Åkerstrand", :start-rating 1500.0}
 {:name "Igor Pilz", :start-rating 1495.04}
 {:name "Isa Solmus", :start-rating 1492.0}
 {:name "Ismail Al-Afifi", :start-rating 1484.75}
 {:name "Jacob", :start-rating 1495.48}
 {:name "Jan-Erik Skoglund", :start-rating 1492.62}
 {:name "Jan-Olov Jansson", :start-rating 1496.0}
 {:name "Joakim Nordlindh", :start-rating 1507.2}
 {:name "Joakim Roggetin", :start-rating 1496.98}
 {:name "Joel Forssell", :start-rating 1496.0}
 {:name "Joel Sjelin", :start-rating 1476.0}
 {:name "Johan Eriksson", :start-rating 1504.28}
 {:name "Johan K", :start-rating 1484.0}
 {:name "Johan Mistell", :start-rating 1500.0}
 {:name "Johan Uselius", :start-rating 1500.0}
 {:name "Johan Utternäs", :start-rating 1496.0}
 {:name "Johan Walkey", :start-rating 1496.0}
 {:name "Jon Brewer", :start-rating 1496.0}
 {:name "Jonny", :start-rating 1496.0}
 {:name "Josef", :start-rating 1500.0}
 {:name "Jukka", :start-rating 1496.0}
 {:name "Kemal Gök", :start-rating 1487.58}
 {:name "Kim C", :start-rating 1500.0}
 {:name "Kimberly", :start-rating 1494.46}
 {:name "Klara Widerström", :start-rating 1490.62}
 {:name "Klas B", :start-rating 1496.0}
 {:name "Lars Larsson", :start-rating 1503.59}
 {:name "Lasse Floberg", :start-rating 1496.0}
 {:name "Lille-Ba Richthoff", :start-rating 1472.0}
 {:name "Madeleine Sandström", :start-rating 1496.0}
 {:name "Magnus Andersson", :start-rating 1523.56}
 {:name "Magnus Envall", :start-rating 1492.0}
 {:name "Marcus Ekholm", :start-rating 1500.0}
 {:name "Martin Adenhav", :start-rating 1492.0}
 {:name "Martin Alm", :start-rating 1597.8}
 {:name "Martin B", :start-rating 1496.0}
 {:name "Mats Elzén", :start-rating 1494.35}
 {:name "Mats Lindberg", :start-rating 1500.0}
 {:name "Mats Sandberg", :start-rating 1499.74}
 {:name "Mattias Tengstrand", :start-rating 1493.63}
 {:name "Maud Akguder", :start-rating 1484.79}
 {:name "Max", :start-rating 1492.0}
 {:name "Mehdi", :start-rating 1500.0}
 {:name "Mehmet Kirik", :start-rating 1592.0}
 {:name "Mehmet Özkan", :start-rating 1511.25}
 {:name "Mia Ebbesson", :start-rating 1500.0}
 {:name "Mikael L Lindgren", :start-rating 1500.0}
 {:name "Mikael Sundqvist", :start-rating 1496.0}
 {:name "Mikael Wemming", :start-rating 1500.0}
 {:name "Milorad", :start-rating 1500.0}
 {:name "Mohsen Gharagozloo", :start-rating 1490.2}
 {:name "Nenne", :start-rating 1494.3}
 {:name "Nenne Grkovic", :start-rating 1481.19}
 {:name "Nicolas Lysandrides", :start-rating 1504.61}
 {:name "Niklas Farhat", :start-rating 1588.29}
 {:name "Nino Poli", :start-rating 1500.0}
 {:name "Olle Bäckgren", :start-rating 1500.0}
 {:name "Patrik Johansson", :start-rating 1500.0}
 {:name "Pelle", :start-rating 1493.94}
 {:name "Pelle Eriksson", :start-rating 1459.0}
 {:name "Pelle Wärngren", :start-rating 1496.0}
 {:name "Pendar Khalati", :start-rating 1493.72}
 {:name "Per", :start-rating 1489.0}
 {:name "Peter Bleckert", :start-rating 1496.0}
 {:name "Peter Hedbrandt", :start-rating 1492.0}
 {:name "Petter Sjöberg", :start-rating 1500.0}
 {:name "Rickard Hellman", :start-rating 1484.0}
 {:name "Rickard Lorenzen", :start-rating 1475.0}
 {:name "Rickard Åkerstrand", :start-rating 1492.0}
 {:name "Rickie Silverhorn", :start-rating 1500.0}
 {:name "Rizgar Kurdi", :start-rating 1661.29}
 {:name "Robert Ricciutti", :start-rating 1500.0}
 {:name "Robert Sundström", :start-rating 1500.0}
 {:name "Roger Samuelsson", :start-rating 1496.0}
 {:name "Roland Sundqvist", :start-rating 1500.0}
 {:name "Ronny Sandström", :start-rating 1486.62}
 {:name "Ronny Selin", :start-rating 1499.73}
 {:name "Said Haffo", :start-rating 1500.0}
 {:name "Sait I", :start-rating 1500.0}
 {:name "Salim Chabo", :start-rating 1500.0}
 {:name "Samir Farhat", :start-rating 1500.0}
 {:name "Samuel Rosendahl", :start-rating 1487.75}
 {:name "Sara Karlsson", :start-rating 1500.0}
 {:name "Sara Strid", :start-rating 1494.53}
 {:name "Shahin Sateei", :start-rating 1494.23}
 {:name "Siavash Hooshmand", :start-rating 1435.66}
 {:name "Souti", :start-rating 1476.0}
 {:name "Stefan Poon", :start-rating 1500.0}
 {:name "Stig Landström", :start-rating 1500.0}
 {:name "Ted Massoud", :start-rating 1492.0}
 {:name "Therese Bäckgren", :start-rating 1496.0}
 {:name "Tim", :start-rating 1500.0}
 {:name "Tim A", :start-rating 1421.0}
 {:name "Tina", :start-rating 1496.0}
 {:name "Turgay", :start-rating 1496.0}
 {:name "Ulf Brinkkjaer", :start-rating 1500.0}
 {:name "Ulf Kristiansson", :start-rating 1517.67}
 {:name "Vian Sadwand", :start-rating 1486.1}
 {:name "Viktor Elm", :start-rating 1489.45}
 {:name "Welat Ektiren", :start-rating 1495.0}
 {:name "Yazdan Shah Hosseini", :start-rating 1448.18}
 {:name "Zeki", :start-rating 1492.0}
 {:name "Zhina Bergström", :start-rating 1517.07}
 {:name "Åsa Starfjord", :start-rating 1496.0}
 {:name "Öner Cicek", :start-rating 1485.38}
 {:name "Örjan Nordhage", :start-rating 1500.0}]
