# Uppsala Backgammon #

Lagrar backgammonresultat för UBGF (föreningsnummer 9999037152).

Rankingansvarig i Stockholm är Magnus Höger (magnus@hoger.com),
här länk till hans [FB-profil](https://www.facebook.com/magnus.hoger).


Links
=====
- [UBGF - Dropbox](https://www.dropbox.com/sh/sw6yinpdb5qwquu/AAA1aeFHudydKyEB2BR1XU0Aa?dl=0&fbclid=IwAR3D4OOW-0RPxjYFn_DRjJvSVbPoEt77fV2FX5f5MBONVjsvLXGZxxp-8-0)
  Lagring av dokument, sådan som inte lagras här.
- [SBGF - Medlemsregister](http://www.sbgf.se/tr/start.aspx?fbclid=IwAR29J0TS3e56Puw9531JF2lop2aRTui4Cr1x2VXvMcapGL6gnh5WC8dk95k)
   Rapportering av ranking till SBGF + möjlighet att söka spelare. Kräver inloggning.
- [Youtube](https://www.youtube.com/channel/UCmxAPhu4HMrlneCtXI1xZkA)
  Alla våra stream:ade matcher.
- [URK](https://www.dropbox.com/s/gqva0ycvjajse9u/lag-BG-Sthlm.xlsx?dl=0&fbclid=IwAR1o_wtJiXhef20LW43uoN-ZjMAHqZbhkUpPQYG7CIUbN1iuLF_topln1SM)
  Spellschema för Uppsalas lag i Stockholmsserien.
- [OBS Studio](https://obsproject.com/sv)
  Program som används för att stream:a matcher. Inställningar finns i Mickes
  katalog på Dropbox under: UBGF\Streaming\OBS Studio\Inställningar  
- [Clojure](https://clojure.org)
  Programmeringsspråket som används för att parse:a de olika filerna, till exampel players.clj
  (.clj är en förkortning för Clojure).
- [Chart.js](https://www.chartjs.org)
  Är det [JavaScript](https://sv.wikipedia.org/wiki/Javascript)-baserade bibliotek som vi använder
  för att generera rating-diagrammen.
- [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#tables)
  Dokumentation av hur Markdown-formatet fungerar, vilket är vad denna README-fil är skriven i (README.md)
  och som genererar den text du just nu läser.
- [FreeMarker](https://freemarker.apache.org/docs/ref.html)
  Beskriver språket som används av mall-filerna i 'templates'-katalogen för att generera filer till 'output'-katalogen
  genom att läsa 'settings/output.clj'.

FreeMarket crash course
=======================
The [FreeMarker](https://freemarker.apache.org/docs/index.html) template engine...

| Name of List |  Attribute   |     Example      |
| ------------ |------------- | ---------------- |
| players      | rank         | ${player.rank}   |
|              | name         | ${player.name}   |
|              | rating       | ${player.rating} |
