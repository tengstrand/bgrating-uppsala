
;; start-rating is the rating the player had 2019-12-31.

[{:name "Aida", :start-rating 1495.19 :alias "Aida"}
 {:name "Albert Baghdasarian", :start-rating 1464.17 :sbgf 2554 :alias "Albert" :profile "albertb.htm"}
 {:name "Alex Yazdani", :start-rating 1448.61 :sbgf 2604 :alias "Alex" :profile "alexy.htm"}
 {:name "Ali Marankoz", :start-rating 1718.86 :sbgf 1972 :color [] :alias "Ali" :profile "ali.htm"}
 {:name "Anders Lövgren", :start-rating 1500.0 :alias "Anders L"}
 {:name "Annica Beil", :start-rating 1495.27 :sbgf 2597 :alias "Annica B"}
 {:name "Axel", :start-rating 1494.51 :alias "Axel"}
 {:name "Azam Kabire", :start-rating 1309.67 :sbgf 2320 :alias "Azam" :profile "azamk.htm"}
 {:name "Björn Ekström", :start-rating 1498.07 :alias "Björn E"}
 {:name "Charlie Victor", :start-rating 1452.96 :sbgf 2485 :alias "Charlie" :profile "charlie.htm"}
 {:name "Cilla Clark", :start-rating 1444.29 :sbgf 1942 :alias "Cilla" :profile "cilla.htm"}
 {:name "Fredrik Lindström", :start-rating 1527.2 :sbgf 2208 :alias "Fredrik L" :profile "fredrikl.htm"}
 {:name "Fredrik Ångman", :start-rating 1497.8 :sbgf 2553 :alias "Fredrik Å" :profile "fredrika.htm"}
 {:name "Garnik Babakhanlou", :start-rating 1501.63  :alias "Garnik B" :profile "garnikb.htm"}
 {:name "Garnik Shahbandari", :start-rating 1491.04 :sbgf 2659 :alias "Garnik S" :profile "garniks.htm"}
 {:name "Grigore Wiklund", :start-rating 1480.38 :sbgf 2652 :alias "Grishan" :profile "grishan.htm"}
 {:name "Jens Berger", :start-rating 1496.62 :alias "Jens B"}
 {:name "Joakim Tengstrand", :start-rating 1677.21 :sbgf 2421 :alias "Jocke" :profile "joakim.htm"}
 {:name "Johan Norberg", :start-rating 1618.04 :sbgf 2486 :alias "Johan" :profile "johan.htm"}
 {:name "Jonas Bergkvist", :start-rating 1546.76 :sbgf 2530 :alias "Jonas" :profile "jonas.htm"}
 {:name "Josef Solymani", :start-rating 1495.07 :alias "Josef S"}
 {:name "Laila Leonhardt", :start-rating 1512.47 :sbgf 2036 :alias "Laila" :profile "laila.htm"}
 {:name "Lars Järdö", :start-rating 1518.8 :sbgf 2532 :alias "Lars J" :profile "larsj.htm"}
 {:name "Mahmoud Gramin", :start-rating 1538.8 :sbgf 1516 :alias "Mahmoud" :profile "mahmoud.htm"}
 {:name "Marianne Lind", :start-rating 1495.9 :alias "Marianne L"}
 {:name "Mattias Rosendahl", :start-rating 1535.59 :sbgf 1951 :alias "Mattias" :profile "mattias.htm"}
 {:name "Meruzhan", :start-rating 1500.0 :alias "Meruzhan"}
 {:name "Mikael Nilsson", :start-rating 1659.18 :sbgf 1790 :alias "Micke" :profile "micke.htm"}
 {:name "Mika Olavi", :start-rating 1491.08 :alias "Mika O"}
 {:name "Patrick Kimari", :start-rating 1743.2 :sbgf 2487 :alias "Patrick" :profile "patrick.htm"}
 {:name "Peyman Solymani", :start-rating 1525.49 :sbgf 2657 :alias "Peyman" :profile "peyman.htm"}
 {:name "Razmik Mehrabi", :start-rating 1500.0 :sbgf 2666 :alias "Razmik" :profile "razmik.htm"}
 {:name "Robert Karlsson", :start-rating 1449.5 :sbgf 2596 :alias "Robert" :profile "robertk.htm"}
 {:name "Roland Sahlén", :start-rating 1500.0 :sbgf 252 :alias "Roland"}
 {:name "Sandra Blixt-Fjellner", :start-rating 1489.63 :sbgf 2589 :alias "Sandra BF"}
 {:name "Simon Olsson", :start-rating 1327.04 :sbgf 2104 :alias "Simon" :profile "simono.htm"}
 {:name "Stefan Hogeborn", :start-rating 1486.46 :sbgf 2536 :alias "Stefan Ho" :profile "stefanhog.htm"}
 {:name "Stefan Hörbing", :start-rating 1498.21 :sbgf 2506 :alias "Stefan Hö" :profile "stefanhor.htm"}
 {:name "Sture Lifh", :start-rating 1500.0 :sbgf 554 :alias "Sture L"}
 {:name "Åsa Georgson", :start-rating 1500.0 :alias "Åsa G"}
 {:name "Åsa Söderberg", :start-rating 1501.22 :sbgf 2535 :alias "Åsa S"}]
