
{:name "UBGF Torsdagsturnering VT#3"
 :poängjakt? true
 :rounds [{:round 1
           :length 7
           :matches [["Albert" "Mahmoud"]
                     ["Grishan" "Ali"]
                     ["Micke" "Robert"]
                     ["Azam" "Johan"]
                     ["Fredrik Å" "Alex"]
                     ["Jocke" "Bye"]
                     ["Cilla" "Bye"]
                     ["Garnik S" "Bye"]]}
          {:round 2
           :length 7
           :matches [["Albert" "Ali"]
                     ["Micke" "Johan"]
                     ["Fredrik Å" "Jocke"]
                     ["Cilla" "Garnik S"]]}
          {:round 3
           :length 7
           :matches [["Ali" "Johan"]
                     ["Fredrik Å" "Garnik S"]]}
          {:round 4
           :length 9
           :matches [["Johan" "Garnik S"]]
           :winner "Johan"}]
 :info {[1 "Micke" "Robert"] {:stream "https://www.youtube.com/watch?v=oIbozji9PGM"
                              :xg "w04-2020-01-23-Micke-Robert-torsdagsturnering-7p-089-vs-218.xg"}
        [1 "Johan" "Azam"] {:xg "w04-2020-01-23-Johan-Azam-torsdagsturnering-7p-076-vs-271.xg"}
        [2 "Fredrik" "Jocke"] {:xg "w04-2020-01-23-Fredrik-Jocke-torsdagsturnering-7p-144-vs-057.xg"}
        [2 "Johan" "Micke"] {:stream "https://www.youtube.com/watch?v=JkmjYp-hfrk"
                             :xg "w04-2020-01-23-Johan-Micke-torsdagsturnering-7p-084-vs-158.xg"}
        [3 "Johan" "Ali"] {:stream "https://www.youtube.com/watch?v=aomK-iNTeOc"
                           :xg "w04-2020-01-23-Johan-Ali-torsdagsturnering-7p-071-vs-084.xg"}
        [4 "Johan" "Garnik S"] {:stream "https://www.youtube.com/watch?v=fyTN0rmTTE0"
                                :xg "w04-2020-01-23-Johan-GarnikS-torsdagsturnering-9p-062-vs-167.xg"}}}
