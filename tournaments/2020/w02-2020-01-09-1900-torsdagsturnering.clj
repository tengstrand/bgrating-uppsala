
; http://www.sbgf.se/kalender/resultat.aspx?id=2482&type=3&aPart=10&fbclid=IwAR1WqNoBhEWs8_jkY40sVBf7p7YsaFWIhu5ufoLmxGJ2cpHZ2iIWfXBQsXk

{:name "UBGF Torsdagsturnering VT#1"
 :poängjakt? true
 :rounds [{:round 1
           :length 7
           :matches [["Cilla" "Bye"]
                     ["Garnik S" "Bye"]
                     ["Patrick" "Åsa G"]
                     ["Robert" "Bye"]
                     ["Johan" "Anders L"]
                     ["Albert" "Bye"]
                     ["Micke" "Bye"]
                     ["Jocke" "Ali"]]}
          {:round 2
           :length 7
           :matches [["Cilla" "Garnik S"]
                     ["Patrick" "Robert"]
                     ["Johan" "Albert"]
                     ["Micke" "Ali"]]}
          {:round 3
           :length 7
           :matches [["Cilla" "Patrick"]
                     ["Johan" "Micke"]]}
          {:round 4
           :length 9
           :matches [["Johan" "Patrick"]]
           :winner "Johan"}]
  :info {[1 "Jocke" "Ali"] {:stream "https://www.youtube.com/watch?v=KYAxOjihYUk"
                            :xg "w02-2020-01-09-Ali-Jocke-torsdagsturnering-7p-055-vs-116.xg"}
         [1 "Johan" "Anders L"] {:stream "w02-2020-01-09-Anders-L-Johan-torsdagsturnering-7p-329-vs-024.xg"
                                 :xg "w02-2020-01-09-AndersL-Johan-torsdagsturnering-7p-329-vs-024.xg"}
         [2 "Patrick" "Robert"] {:xg "w02-2020-01-09-Patrick-Robert-torsdagsturnering-7p-115-vs-784.xg"}
         [2 "Micke" "Ali"] {:xg "w02-2020-01-09-Micke-Ali-torsdagsturnering-7p-104-vs-067.xg"}
         [2 "Johan" "Albert"] {:xg "w02-2020-01-09-Johan-Albert-torsdagsturnering-7p-029-vs-118.xg"}
         [3 "Patrick" "Cilla"] {:xg "w02-2020-01-09-Patrick-Cilla-torsdagsturnering-7p-084-vs-155.xg"}
         [3 "Johan" "Micke"] {:stream "https://www.youtube.com/watch?v=A0gutOTwERc"
                              :xg "w02-2020-01-09-Johan-Micke-torsdagsturnering-7p-057-vs-042.xg"}
         [4 "Johan" "Patrick"] {:xg "w02-2020-01-09-Johan-Patrick-torsdagsturnering-9p-105-vs-049.xg"}}}
