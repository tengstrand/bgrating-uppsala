
{:name "Uppsalamästerskapet 2020"
 :poängjakt? false
 :rounds [{:round 1
           :length 15
           :matches [["Patrick" "Mattias"]
                     ["Micke" "Johan"]
                     ["Ali" "Mahmoud"]
                     ["Jocke" "Jonas"]]}
          {:round 2
           :length 17
           :matches [["Mattias" "Johan"]
                     ["Ali" "Jocke"]]}
          {:round 3
           :length 19
           :matches [["Johan" "Ali"]]
           :winner "Johan"}]
 :info {[1 "Mattias" "Patrick"] {:xg "w04-2020-01-25-Mattias-Patrick-uppsalamästerskapet-15p-111-vs-068.xg"}
        [1 "Jocke" "Jonas"] {:xg "w04-2020-01-25-Jocke-Jonas-uppsalamästerskapet-15p-065-vs-125.xg"}
        [1 "Johan" "Micke"] {:stream "https://www.youtube.com/watch?v=G1wbUyU-3-A"
                             :xg "w04-2020-01-25-Johan-Micke-uppsalamästerskapet-15p-056-vs-069.xg"}
        [2 "Johan" "Mattias"] {:stream "https://www.youtube.com/watch?v=GasWrSJnk_s"
                               :xg "w04-2020-01-25-Johan-Mattias-uppsalamästerskapet-17p-071-vs-093.xg"}
        [2 "Ali" "Jocke"] {:xg "w04-2020-01-25-Ali-Jocke-uppsalamästerskapet-17p-102-vs-126.xg"}
        [3 "Johan" "Ali"] {:stream "https://www.youtube.com/watch?v=lXoTHg7IrCA"
                           :xg "w04-2020-01-25-Johan-Ali-uppsalamästerskapet-19p-054-vs-035.xg"}}}
