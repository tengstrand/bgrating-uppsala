
; http://www.sbgf.se/kalender/resultat.aspx?id=2479&type=0&aPart=14&fbclid=IwAR1fDcKCkYqNfldScY7NC7YFHsqcHP_9EL1dhavOu_QpCRdfvhYBpXgX5A4

{:name "UBGF Nyårsrullen - Main"
 :poängjakt? false
 :rounds [{:round 1
           :length 13
           :matches [["Robert" "Patrick"]
                     ["Cilla" "Mahmoud"]
                     ["Stefan Ho" "Jocke"]
                     ["Charlie" "Micke"]
                     ["Stefan Hö" "Roland"]
                     ["Ali" "Åsa S"]
                     ["Albert" "Bye"]
                     ["Mattias" "Bye"]]}
          {:round 2
           :length 13
           :matches [["Patrick" "Mahmoud"]
                     ["Jocke" "Charlie"]
                     ["Roland" "Ali"]
                     ["Albert" "Mattias"]]}
          {:round 3
           :length 13
           :matches [["Mahmoud" "Jocke"]
                     ["Roland" "Albert"]]}
          {:round 4
           :length 13
           :matches [["Jocke" "Roland"]]
           :winner "Roland"}]
 :info {[1 "Charlie" "Micke"] {:stream "https://www.youtube.com/watch?v=H44gY7V0xsw"
                               :xg "w01-2020-01-04-Micke-Charlie-nyårsrullen-main-13p-091-vs-093.xg"}
        [2 "Roland" "Ali"] {:stream "https://www.youtube.com/watch?v=Q3IEBDJutfQ"
                            :xg "w01-2020-01-04-Roland-Ali-nyårsrullen-main-13p-018-vs-042.xg"}
        [3 "Roland" "Albert"] {:stream "https://www.youtube.com/watch?v=BM_Hcfkiqzk"
                               :xg "w01-2020-01-04-Roland-Albert-nyårsrullen-main-13p-054-vs-155.xg"}
        [4 "Roland" "Jocke"] {:stream "https://www.youtube.com/watch?v=gO07_6evnFM"
                              :xg "w01-2020-01-04-Roland-Jocke-nyårsrullen-main-13p-046-vs-054.xg"}}}
