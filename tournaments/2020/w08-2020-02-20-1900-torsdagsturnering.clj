
{:name "UBGF Torsdagsturnering VT#7"
 :poängjakt? true
 :rounds [{:round 1
           :length 7
           :matches [["Anders L" "Robert"]
                     ["Micke" "Azam"]
                     ["Ali" "Meruzhan"]
                     ["Cilla" "Patrick"]
                     ["Alex" "Bye"]
                     ["Albert" "Bye"]
                     ["Garnik S" "Razmik"]
                     ["Jocke" "Bye"]]}
          {:round 2
           :length 7
           :matches [["Robert" "Micke"]
                     ["Ali" "Patrick"]
                     ["Alex" "Albert"]
                     ["Garnik S" "Jocke"]]}
          {:round 3
           :length 7
           :matches [["Micke" "Ali"]
                     ["Alex" "Garnik S"]]}
          {:round 4
           :length 9
           :matches [["Ali" "Garnik S"]]
           :winner "Ali"}]
 :info {[1 "Micke" "Azam"] {:stream "https://www.youtube.com/watch?v=1pKqsVhiKG0"
                            :xg "w08-2020-02-20-Micke-Azam-torsdagsturnering-7p-066-vs-251.xg"}
        [2 "Micke" "Robert"] {:stream "https://www.youtube.com/watch?v=0A1qejVEVbI"
                              :xg "w08-2020-02-20-Micke-Robert-torsdagsturnering-7p-054-vs-128.xg"}
        [2 "Garnik S" "Jocke"] {:xg "w08-2020-02-20-Jocke-GarnikS-torsdagsturnering-7p-146-vs-247.xg"}
        [2 "Ali" "Patrick"] {:xg "w08-2020-02-20-Ali-Patrick-torsdagsturnering-7p-050-vs-100.xg"}
        [3 "Ali" "Micke"] {:stream "https://www.youtube.com/watch?v=Hkt3uTdZFyA"
                           :xg "w08-2020-02-20-Ali-Micke-torsdagsturnering-7p-089-vs-071.xg"}
        [4 "Ali" "Garnik S"] {:xg "w08-2020-02-20-Ali-GarnikS-torsdagsturnering-7p-080-vs-324.xg"}}}
