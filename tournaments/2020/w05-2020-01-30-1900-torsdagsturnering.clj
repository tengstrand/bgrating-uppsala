
{:name "UBGF Torsdagsturnering VT#4"
 :poängjakt? true
 :rounds [{:round 1
           :length 7
           :matches [["Razmik" "Johan"]
                     ["Ali" "Micke"]
                     ["Albert" "Robert"]
                     ["Patrick" "Azam"]
                     ["Mahmoud" "Grishan"]
                     ["Cilla" "Bye"]
                     ["Jocke" "Bye"]
                     ["Garnik S" "Bye"]]}
          {:round 2
           :length 7
           :matches [["Johan" "Micke"]
                     ["Robert" "Patrick"]
                     ["Mahmoud" "Cilla"]
                     ["Jocke" "Garnik S"]]}
          {:round 3
           :length 7
           :matches [["Micke" "Patrick"]
                     ["Mahmoud" "Jocke"]]}
          {:round 4
           :length 9
           :matches [["Micke" "Jocke"]]
           :winner "Jocke"}]
 :info {[1 "Patrick" "Azam"] {:xg "w05-2020-01-30-Patrick-Azam-torsdagsturnering-7p-061-vs-700.xg"}
        [1 "Johan" "Razmik"] {:xg "w05-2020-01-30-Johan-Razmik-torsdagsturnering-7p-048-vs-314.xg"}
        [1 "Micke" "Ali"] {:stream "https://www.youtube.com/watch?v=IGTf_u_T8qU&fbclid=IwAR2988Qm5C69ypV2nt7B1SOE0272ZHJR6-i7WE05UvDb3Widy05fSk8CfPE"
                           :xg "w05-2020-01-30-Micke-Ali-torsdagsturnering-7p-027-vs-061.xg"}
        [2 "Patrick" "Robert"] {:xg "w05-2020-01-30-Patrick-Robert-torsdagsturnering-7p-037-vs-157.xg"}
        [2 "Johan" "Micke"] {:stream "https://www.youtube.com/watch?v=NmSmnbka77A"
                             :xg "w05-2020-01-30-Micke-Johan-torsdagsturnering-7p-064-vs-031.xg"}
        [3 "Jocke" "Mahmoud"] {:stream "https://www.youtube.com/watch?v=YBXQHPASPTc"
                               :xg "w05-2020-01-30-Jocke-Mahmoud-torsdagsturnering-7p-069-vs-141.xg"}
        [3 "Micke" "Patrick"] {:stream "https://www.youtube.com/watch?v=W3HrsuNnFG8"
                               :xg "w05-2020-01-30-Micke-Patrick-torsdagsturnering-7p-048-vs-051.xg"}
        [4 "Jocke" "Micke"] {:stream "https://www.youtube.com/watch?v=UfokCwEtSHo"
                             :xg "w05-2020-01-30-Jocke-Micke-torsdagsturnering-9p-175-vs-084.xg"}}}
