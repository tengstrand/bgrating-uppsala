
; http://www.sbgf.se/kalender/resultat.aspx?id=2480&type=3&aPart=12&fbclid=IwAR2HqXDDcI6huslg5B9rSIJMLM3LYSGiyqSdjGBBIyZQ9J3-NEivMAsyIdA

{:name "UBGF Nyårsrullen - Consolation"
 :poängjakt? false
 :rounds [{:round 1
           :length 9
           :matches [["Robert" "Cilla"]
                     ["Stefan Ho" "Micke"]
                     ["Stefan Hö" "Åsa S"]]}
          {:round 2
           :length 9
           :matches [["Mattias" "Cilla"]
                     ["Ali" "Stefan Ho"]
                     ["Charlie" "Stefan Hö"]
                     ["Patrick" "Bye"]]}
          {:round 3
           :length 9
           :matches [["Mattias" "Ali"]
                     ["Charlie" "Patrick"]]}
          {:round 4
           :length 9
           :matches [["Albert" "Ali"]
                     ["Patrick" "Mahmoud"]]}
          {:round 5
           :length 9
           :matches [["Ali" "Mahmoud"]]
           :winner "Ali"}]}
