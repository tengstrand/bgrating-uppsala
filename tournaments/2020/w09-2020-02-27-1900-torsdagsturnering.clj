
{:name "UBGF Torsdagsturnering VT#8"
 :poängjakt? true
 :rounds [{:round 1
           :length 7
           :matches [["Charlie" "Johan"]
                     ["Ali" "Cilla"]
                     ["Robert" "Jocke"]
                     ["Mahmoud" "Alex"]
                     ["Micke" "Bye"]
                     ["Azam" "Bye"]
                     ["Razmik" "Bye"]
                     ["Fredrik Å" "Bye"]]}
          {:round 2
           :length 7
           :matches [["Johan" "Ali"]
                     ["Jocke" "Alex"]
                     ["Micke" "Azam"]
                     ["Razmik" "Fredrik Å"]]}
          {:round 3
           :length 7
           :matches [["Ali" "Jocke"]
                     ["Micke" "Fredrik Å"]]}
          {:round 4
           :length 9
           :matches [["Ali" "Micke"]]
           :winner "Ali"}]
 :info {[1 "Charlie" "Johan"] {:xg "w09-2020-02-27-Johan-Charlie-torsdagsturnering-059-vs-107.xg"
                               :stream "https://www.youtube.com/watch?v=5tiaUV6__n8"}
        [1 "Robert" "Jocke"] {:xg "w09-2020-02-27-Jocke-Robert-torsdagsturnering-7p-035-vs-152.xg"}
        [2 "Micke" "Azam"] {:stream "https://www.youtube.com/watch?v=1rqGCJW_BZM"
                            :xg "w09-2020-02-27-Micke-Azam-torsdagsturnering-7p-078-vs-187.xg"}
        [2 "Jocke" "Alex"] {:xg "w09-2020-02-27-Jocke-Alex-torsdagsturnering-7p-081-vs-317.xg"}
        [2 "Ali" "Johan"] {:xg "w09-2020-02-27-Ali-Johan-torsdagsturnering-7p-090-vs-084.xg"}
        [3 "Micke" "Fredrik Å"] {:stream "https://www.youtube.com/watch?v=s5-LEluQY0c"
                                 :xg "w09-2020-02-27-Micke-FredrikÅ-torsdagsturnering-7p-060-vs-213.xg"}
        [3 "Ali" "Jocke"] {:xg "w09-2020-02-27-Ali-Jocke-torsdagsturnering-7p-051-vs-104.xg"}
        [4 "Ali" "Micke"] {:stream "https://www.youtube.com/watch?v=qtGwmQSx4P4"
                           :xg "w09-2020-02-27-Ali-Micke-torsdagsturnering-9p-040-vs-098.xg"}}}
