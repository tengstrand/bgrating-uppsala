
{:name "UBGF Torsdagsturnering VT#6"
 :poängjakt? true
 :rounds [{:round 1
           :length 7
           :matches [["Grishan" "Albert"]
                     ["Jocke" "Micke"]
                     ["Garnik S" "Razmik"]
                     ["Cilla" "Johan"]
                     ["Robert" "Ali"]
                     ["Azam" "Alex"]
                     ["Mahmoud" "Bye"]
                     ["Stefan Hö" "Bye"]]}
          {:round 2
           :length 7
           :matches [["Albert" "Jocke"]
                     ["Razmik" "Johan"]
                     ["Ali" "Alex"]
                     ["Mahmoud" "Stefan Hö"]]}
          {:round 3
           :length 7
           :matches [["Jocke" "Razmik"]
                     ["Ali" "Mahmoud"]]}
          {:round 4
           :length 9
           :matches [["Jocke" "Mahmoud"]]
           :winner "Jocke"}]
 :info {[1 "Cilla" "Johan"] {:stream "https://www.youtube.com/watch?v=IuVto4IH6E0"
                             :xg "w07-2020-02-13-Cilla-Johan-torsdagsturnering-7p-141-vs-055.xg"}
        [2 "Johan" "Razmik"] {:stream "https://www.youtube.com/watch?v=8DGX-4sRqsg"
                              :xg "w07-2020-02-13-Johan-Razmik-torsdagsturnering-7p-040-vs-114.xg"}
        [2 "Albert" "Jocke"] {:stream "https://www.youtube.com/watch?v=27ZF-RdKYoQ"
                              :xg "w07-2020-02-13-Jocke-Albert-torsdagsturnering-7p-048-vs-159.xg"}
        [3 "Jocke" "Razmik"] {:xg "w07-2020-02-13-Jocke-Razmik-torsdagsturnering-7p-145-vs-215.xg"}
        [4 "Jocke" "Mahmoud"] {:xg "w07-2020-02-13-Jocke-Mahmoud-torsdagsturnering-7p-101-vs-143.xg"}}}
