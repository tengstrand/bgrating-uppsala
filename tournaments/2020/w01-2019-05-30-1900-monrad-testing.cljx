
{:name "UBGF Torsdagsturnering (monrad)"
 :poängjakt? false
 :rounds [{:round 1
           :length 7
           :matches [["Cilla" "Robert"]
                     ["Mahmoud" "Ali"]]
           :winners #{"Robert"
                      "Ali"}}
          {:round 2
           :length 7
           :matches [["Cilla" "Mahmoud"]
                     ["Alex" "Robert"]]
           :winners #{"Mahmoud"
                      "Robert"}}
          {:round 3
           :length 7
           :matches [["Ali" "Cilla"]
                     ["Mahmoud" "Alex"]]
           :winners #{"Ali"
                      "Alex"}}
          {:round 4
           :length 7
           :matches [["Cilla" "Alex"]
                     ["Ali" "Robert"]]
           :winners #{"Alex"
                      "Robert"}}
          {:round 5
           :length 7
           :matches [["Mahmoud" "Robert"]
                     ["Ali" "Alex"]]
           :winners #{"Mahmoud"
                      "Alex"}}
          {:round 6
           :length 7
           :matches [["Alex" "Mahmoud"]]
           :winner "Alex"}]}
