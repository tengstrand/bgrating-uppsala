
{:name "UBGF Torsdagsturnering VT#5"
 :poängjakt? true
 :rounds [{:round 1
           :length 7
           :matches [["Robert" "Fredrik Å"]
                     ["Cilla" "Mahmoud"]
                     ["Jocke" "Grishan"]
                     ["Ali" "Johan"]
                     ["Albert" "Azam"]
                     ["Garnik S" "Bye"]
                     ["Bye" "Alex"]
                     ["Micke" "Bye"]]}
          {:round 2
           :length 7
           :matches [["Robert" "Mahmoud"]
                     ["Jocke" "Johan"]
                     ["Azam" "Garnik S"]
                     ["Alex" "Micke"]]}
          {:round 3
           :length 7
           :matches [["Mahmoud" "Jocke"]
                     ["Garnik S" "Micke"]]}
          {:round 4
           :length 9
           :matches [["Jocke" "Garnik S"]]
           :winner "Garnik S"}]
 :info {[1 "Jocke" "Grishan"] {:xg "w06-2020-02-06-Grishan-Jocke-torsdagsturnering-7p-155-vs-152.xg"}
        [1 "Johan" "Ali"] {:xg "w06-2020-02-06-Johan-Ali-torsdagsturnering-7p-069-vs-139.xg"}
        [2 "Micke" "Alex"] {:stream "https://www.youtube.com/watch?v=_7i38IQ1j2s"
                            :xg "w06-2020-02-06-Micke-Alex-torsdagsturnering-7p-068-vs-111.xg"}
        [2 "Jocke" "Johan"] {:xg "w06-2020-02-06-Jocke-Johan-torsdagsturnering-7p-093-vs-055.xg"}
        [3 "Garnik S" "Micke"] {:stream "https://www.youtube.com/watch?v=OpV-Be8HtzU"
                                :xg "w06-2020-02-06-Micke-GarnikS-torsdagsturnering-7p-070-vs-140.xg"}
        [3 "Jocke" "Mahmoud"] {:stream "https://www.youtube.com/watch?v=LdMXMBtECUI"
                               :xg "w06-2020-02-06-Jocke-Mahmoud-torsdagsturnering-7p-063-vs-114.xg"}
        [4 "Jocke" "Garnik S"] {:stream "https://www.youtube.com/watch?v=yAJRXz9CI2E"
                                :xg "w06-2020-02-06-Jocke-GarnikS-torsdagsturnering-9p-096-vs-159.xg"}}}
