
{:name "UBGF Torsdagsturnering VT#2"
 :poängjakt? true
 :rounds [{:round 1
           :length 7
           :matches [["Albert" "Micke"]
                     ["Grishan" "Cilla"]
                     ["Simon" "Patrick"]
                     ["Mahmoud" "Jocke"]
                     ["Johan" "Fredrik Å"]
                     ["Alex" "Garnik S"]
                     ["Ali" "Azam"]
                     ["Robert" "Bye"]]}
          {:round 2
           :length 7
           :matches [["Albert" "Cilla"]
                     ["Simon" "Jocke"]
                     ["Fredrik Å" "Alex"]
                     ["Ali" "Robert"]]}
          {:round 3
           :length 7
           :matches [["Cilla" "Simon"]
                     ["Alex" "Ali"]]}
          {:round 4
           :length 9
           :matches [["Cilla" "Ali"]]
           :winner "Ali"}]
 :info {[1 "Simon" "Patrick"] {:xg "w03-2020-01-16-Simon-Patrick-torsdagsturnering-7p-137-vs-085.xg"}
        [1 "Albert" "Micke"] {:xg "w03-2020-01-16-Albert-Micke-torsdagsturnering-7p-220-vs-125.xg"}
        [1 "Fredrik Å" "Johan"] {:xg "w03-2020-01-16-FredrikÅ-Johan-torsdagsturnering-7p-111-vs-075.xg"}
        [1 "Jocke" "Mahmoud"] {:xg "w03-2020-01-16-Jocke-Mahmoud-torsdagsturnering-7p-099-vs-133.xg"}
        [2 "Simon" "Jocke"] {:xg "w03-2020-01-16-Simon-Jocke-torsdagsturnering-7p-157-vs-091.xg"}}}
